import React, { Component } from 'react';
import wishes from './wishes';
import Fade from 'react-reveal/Fade';
import Pulse from 'react-reveal/Pulse';
import './App.css';
import '../node_modules/material-design-icons/iconfont/material-icons.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animate : true,
    }
  }

  render() {
    return (
      <div className="App">
        <div className="col-lg-7 offset-5" style={{marginTop: '30%'}}>
          {wishes.Messages.map((data, key) => (
              <Fade bottom>
                <div className="card shadow" key={key} style={{marginTop: '1.5%', marginBottom: '1.5%'}}>
                  <div className="card-body">
                    <div style={{textAlign: 'left'}}>
                      <div className="row">
                        {data.image === undefined ? (
                          <img src={require(`./pfp.png`)} alt="pfp" style={{borderWidth: 2, borderRadius: 100, height: '10%', width: '10%'}} />
                        ) : (
                          <img src={require(`./gopi_data/${data.image}`)} alt="pfp" style={{borderWidth: 2, borderRadius: 100, height: '10%', width: '10%'}} />
                        )}
                        <div className="col-lg-9">
                          <h6 style={{color: '#3854C0', fontWeight: 500}}>{data.EmpName}</h6>
                          <p style={{fontSize: 14, fontWeight: '500', fontStyle: 'italic', marginTop: '2%'}}>&nbsp;&nbsp;&nbsp;&nbsp;"{data.Comments}"</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Fade>
            ))}
        </div>
      </div>
    );
  }
}

export default App;
