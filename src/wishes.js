let wishes = {
        "Messages": [
            {
                "Comments": "I am delighted to learn that Gopi has completed 25 years at Infosys. Gopi is a stalwart at Infosys. He has played a seminal role in developing the Mangalore Development Centre. I have had the pleasure of working with him on Be The Navigator and it has been an absolute pleasure! Keep Going Gopi! Warm Regards, Nandan",
                "EmpName": "Nandan",
                "Unit": "Leadership",
                "Pic Details": "WITHNANDAN",
                "sdsd": "1st Message",
                "Order": "1",
                "image": "2pic.PNG"
            },
            {
                "Comments": "A man who walks and talks dedication, an exemplar in all that he does, fun-and-frolic personified and a true blue Infoscion - Today is a very special day for us as we celebrate an extraordinary member of the Infosys family – Gopi KK, SVP & Service Offering Head – Oracle, for successfully completing a silver jubilee at Infosys! \r\n\r\nFondly called as ‘Gopinator’ by many, for ‘being the navigator’ from the heart, his attention to detail and consistency play a vital role in delivering dynamic and quality innovation. A friend, guide and mentor to many, Gopi leads his team from the front and motivates those working with him to bring out their best.\r\n\r\nHe has played an integral part in our collective journey as a company so far. On this glorious occasion, let’s take a moment to congratulate and wish him many more remarkable milestones in his stellar career journey! ",
                "EmpName": "U B Pravin",
                "Unit": "Leadership",
                "Order": "2",
                "image": "3pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi on this momentous occasion. Your passion, energy is unparalleled. You are a true BLUE Infosian, who put the success of infosys team above everything. Your passion for Mangalore DC and its cause is commendable. Thanks for the excellent moments and time we shared. Wishing you many years and my wishes to your lovely family.",
                "EmpName": "Dinesh",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP7",
                "sdsd": "2nd Message",
                "Order": "3",
                "image": "4pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi for this significant milestone! Quarter of a century!! Welcome to the club :) - Shaji",
                "EmpName": "Shaji",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP3",
                "Order": "4",
                "image": "5pic.PNG"
            },
            {
                "Comments": "Dear Gopi Warm wishes on a fantastic milestone! May you stay forever young, keep your unique brand of cheer and good will going, and inspire many as you always have. Thank you for your warm friendship over the years. I consider myself privileged for having known you. Best wishes, richard",
                "EmpName": "Richard Lobo",
                "Unit": "Leadership",
                "Pic Details": "DC0",
                "Order": "5",
                "image": "6pic.PNG"
            },
            {
                "Comments": "Dear Gopi, As you complete this impressive milestone and cherish the fond memories, I want to take a moment to wish you the very best in all that life has to offer. Have been working with you in recent years and I am very glad to have known a passionate and a friendly person like you. - Shishank",
                "EmpName": "Shishank Gupta",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP",
                "Order": "6",
                "image": "7pic.PNG"
            },
            {
                "Comments": "Dear Gopi, Congratulations and wish you all the best for future. It has been a pleasure working with you and I look forward to a continued partnership in future as well. This is an incredible achievement, enjoy the moment. Cheers !!! Vishal Salvi",
                "EmpName": "Vishal Salvi",
                "Unit": "Leadership",
                "Order": "7",
                "image": "8pic.PNG"
            },
            {
                "Comments": "Congrats to a superb run ! best wishes Rajesh Varrier",
                "EmpName": "Rajesh Varrier",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP4",
                "Order": "8",
                "image": "9pic.PNG"
            },
            {
                "Comments": "Dear Gopi, Welcome to club25 and congratulations on this illustrious journey and milestone. Wish you many more success and milestones in the journey with Infosys. Thanks, Rajneesh",
                "EmpName": "Rajneesh Malviya",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP10",
                "Order": "9",
                "image": "10pic.PNG"
            },
            {
                "Comments": "Congrats Gopi on a fantastic milestone.. i have known for a large part of the last 2 decades and it has always been a pleasure working with you.. your people connect and empathy for employees stand out in my mind.. of course last few years you have also aptly earned the title of the Gopinator by ably driving our innovation agenda with ZD/BTN.. congrats and wish you many more successful years. - Narry",
                "EmpName": "Narsimha M",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP2",
                "Order": "10",
                "image": "11pic.PNG"
            },
            {
                "Comments": "Congratulations Gops on a momentous occasion! 100 incredible quarters of passion, intellect, joy, madness, learning, giving, leadership and above all a journey of consequence! Thank you for making a difference everyday, for putting the organization above everything else, for setting an example on CLIFE, for the belief, for the relentlessness, for the evolution, for the friendship and for being a true blue Infoscion. It has been an absolute honour working with you. Wishing you the very best in everything you do. nanju",
                "EmpName": "Nanjappa B. S",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP11",
                "Order": "11",
                "image": "12pic.PNG"
            },
            {
                "Comments": "My friend Gopi, many congratulations on this wonderful milestone. this is truly an amazing day but when you look back on 7/11/94 today, it will feel so unreal to believe that you would be celebrating 25 years on 7/11/19.. I am sure you cherish all these years of hard work, friendships, achievements, memories and experiences. It is fascinating on how you have contributed and made a huge difference to Infosys through so many ways. You have lived the value system and you are truly a role model for the new Infoscions who are starting their career. All the very best to you for the future - both professionally and personally. -ARUN",
                "EmpName": "Arun Hoskere",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP",
                "Order": "12",
                "image": "13pic.PNG"
            },
            {
                "Comments": "Gopi, Congratulations on completing 25 years in Infosys. I have known you from 2000 when we worked together in Toronto. To me you personify the quote - “Work hard, play hard” You are extremely committed to everything that you do at work and at the same time fun person outside work with a great sense of humor. Wish you the very best for many more years of success.. Bali.",
                "EmpName": "Balakrishna DR",
                "Unit": "Leadership",
                "Pic Details": "LEADERSHIP",
                "Order": "13",
                "image": "14pic.PNG"
            },
            {
                "Comments": "Congratulations on reaching this major milestone Gopi! Its been great working alongside you and your cheerful attitude is contagious! At the same time, the seriousness and passion you bring in your work and to your teams is impressive. Enjoy the moment, and best of luck for a successful future ahead. Deepak Padaki",
                "EmpName": "Deepak Padaki",
                "Unit": "Leadership",
                "Order": "14",
                "image": "15pic.PNG"
            },
            {
                "Comments": "Hi Gopi, congratulations on this phenomenal milestone! Thank you for your contributions across these years. In all my interactions, I have found you to be a pleasant and ever-smiling person, but with a deep commitment to achieve big results. You are one of those people who have been great corporate citizens- doing more for the company than just your work! Wishing you the very best for many more years here! Cheers Krish",
                "EmpName": "Krish Shankar",
                "Unit": "Leadership",
                "Order": "15",
                "image": "16pic.PNG"
            },
            {
                "Comments": "Great to see GOPIKK brand flying high . One of the most positive and humble leader at Infosys. All the best, my friend. Regards Munjay",
                "EmpName": "Munjay Singh",
                "Unit": "Leadership",
                "Pic Details": "FRIEND",
                "Order": "16",
                "image": "17pic.PNG"
            },
            {
                "Comments": "Hey Gopi Wow! 25 years! What a amazing journey you have had in Infosys so far. You have donned multiple hats and excelled in each of them - DC head, business leader of multiple units, and as the \"Gopinator\" for driving BTN, Your energy, passion, innovation and commitment are an inspiration to many welcome to the 25 club buddy and wish you many more rocking years. - Nabarun",
                "EmpName": "Nabarun",
                "Unit": "Leadership",
                "Order": "16",
            },
            {
                "Comments": "25 years, 300 months, 1,304 weeks, 9,132 days, 219,168 hours, 1315080 minutes!!\r\nWow, what a memorable and enviable milestone!! You have played many different roles all these years not just related to work but also as a guide, motivator, friend, mentor and what not.  You have made yourself available to many of us when we needed.\r\n\r\nOn behalf of all of us at mInfy, Happy Service Anniversary Gopi!\r\n\r\nWe are proud to have you at our DC.   Thank you for all the hard work, dedication n commitment put in for the past 25 years to take Infosys to greater heights and also the role that you have played in bringing growth and visibility for the Mangalore DC. May success follow you everywhere you go, in the halo of prosperity may you grow,may you get everything that you could ever ask for, good health, peace and joy always!\r\n",
                "EmpName": "Vasu",
                "Unit": "DC",
                "Pic Details": "DC19",
                "sdsd": "1st Message",
                "Order": "17",
                "image": "19pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi. All the very best. - Mahesh Kamath",
                "EmpName": "Mahesh Kamath P",
                "Unit": "DC",
                "Pic Details": "DC4",
                "Order": "18",
                "image": "20pic.PNG"
            },
            {
                "Comments": "Dear Gopi, You have been a pillar of inspiration for many around you. Your energy and passion on anything you do are second to none. Wishing you many more years of being a source of light to folks around you. Ashish",
                "EmpName": "Ashish Agarwal",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "19",
                "image": "21pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi sir.. Wishing you many more successful years with Infosys - Deepak Kamath",
                "EmpName": "Deepak Kamath N",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "20",
                "image": "22pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi on this phenomenal milestone! Your ever-smiling, never-give-up, always-focus-on-result attitude is an inspiration to many of us. I wish you many more successful milestones ahead in life and career. - Rajaneesh",
                "EmpName": "Rajaneesh K Rao",
                "Unit": "DC",
                "Order": "21",
            },
            {
                "Comments": "Dear Gopi Congratulation on this once in lifetime Milestone. Your passion and commitment is truly praise worthy. The way you grew EAIS practice is a true testament of your passionate leadership. You have been one of the TRUE pillars of BTN. The way you transformed the whole BTN initiative into a mass revolution is truly adorable and if you ask people envy you for the same. You always take care of people irrespective of the location and hierarchy. Wishing you many more such milestone, success and good health. Regards, Rajesh Pai",
                "EmpName": "Rajesh Pai",
                "Unit": "DC",
                "Order": "22",
                "image": "24pic.PNG"
            },
            {
                "Comments": "Hey Gopi, Congratulations on completing 25 years at Infosys...indeed a significant milestone. You have been an inspiration to a lot many of us at work and a driving force to growing Minfosys and sustaining it at this level till date. You energy never ceases to subside with age and that is what makes you the real Gopinator. Wishing you Good Luck and tons of success ahead! - Roshan",
                "EmpName": "Roshan L. Dsouza",
                "Unit": "DC",
                "Pic Details": "DC3",
                "Order": "23",
                "image": "25pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi on the 25th year milestone. You have been an inspiring leader and was amazed to see how the ZD/BTN was run at org level becoming one of the most liked and talked about initiative. It has been great experience working you with you. And I wish you many more success. Wish you all the best. - Shobha",
                "EmpName": "Shobha M",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "24",
                "image": "26pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi on your 25th Service Anniversary. You are an inspiring leader for many of us to look up to. The continuous passion and conviction with which you drive new initiatives, and motivate your team is really admirable. Wishing you many more successful years ahead, both on personal and professional front! -Hareesh P R",
                "EmpName": "Hareesh P R",
                "Unit": "DC",
                "Pic Details": "DC3",
                "Order": "25",
                "image": "27pic.PNG"
            },
            {
                "Comments": "Thanks Gopi for all your leadership in driving growth of Infosys business and leading some of the critical strategic Corp initiatives. Your are an inspirational leader, your passion to work is just contagious. It is fun to work with you - wishing you many more years of success @Infy - Kumar Shenoy",
                "EmpName": "Kumar Shenoy",
                "Unit": "DC",
                "Pic Details": "DC11",
                "Order": "26",
            },
            {
                "Comments": "Hi Gopi, This is indeed a splendid milestone. Congratulations on completing 25 years in Infosys. I have been seeing you since i joined Infosys in 1998 and there is not a bit of change in you. I still see the same rigour and enthusiasm in you. Thanks to you for all the support provided directly or indirectly. I wish you all the very best for your future and hope to see you complete 50 years in Infosys. Best Regards, Sanga.",
                "EmpName": "Sangameshwar Pujari",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "27",
                "image": "29pic.PNG"
            },
            {
                "Comments": "Gopi - You are a great role model and source of motivation, passion and energy. Wish you all the very best, kkb",
                "EmpName": "Krishnakumar Bharathan",
                "Unit": "DC",
                "Pic Details": "DC2",
                "Order": "28",
                "image": "30pic.PNG"
            },
            {
                "Comments": "Congratulations Gopi, wishing you many more fruitful years ahead :) - Ganesh N S",
                "EmpName": "Ganesh Nekkare Subrahmanya",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "29",
                "image": "31pic.PNG"
            },
            {
                "Comments": "Hey Gopi, Congratulations!! Great Milestone. 25yrs of love life with Infosys is a great commitment. Wish you many more great milestones ahead. Your energy and Charisma always Inspires me. - Manoj Thaleppady",
                "EmpName": "Manoj Kumar Thaleppady",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "30",
                "image": "32pic.PNG"
            },
            {
                "Comments": "Wish you many more successful milestones and life's best at Infosys, Gopi. Have had the opportunity to interact at the DC offsite this year. Martin",
                "EmpName": "Martin Gabriel",
                "Unit": "DC",
                "Pic Details": "DC19",
                "Order": "31",
            },
            {
                "Comments": "My description of you in words. Lively, Vibrant, Fun-loving, Down to Earth, Young at heart, Suave, Nattily dressed, lots of panache, Focused, Orator, Motivator,Best friend- Mirror:) and the list is endless. But the summary is simple and straight- A great guy to hang around and have around. Pleasure to know you, Gopi. May god bless you always and every time. All the very best and wishing you lots of sunshine, joy and fun. - Suresh",
                "EmpName": "Suresh M Kunnath",
                "Unit": "DC",
                "Pic Details": "DC5",
                "Order": "32",
                "image": "34pic.PNG"
            },
            {
                "Comments": "Dear Gopi, Congratulations on this amazing milestone – 25, while time will take care of such milestones, people like you make it special! Thank you for being a friend, mentor, leader and being there every step of the way. Of the 25, we have known each other and worked together many years and memories have an abundance of positivity built on trust, personal affection and a strong professional bond. Your 25 years association with Infosys has seen the best of your charismatic leadership – driven by values, through conviction, empowerment, commitment & passion. You motivate others to dream and make it a reality. Personally I feel that you foster a sense of belief in people to see beyond the ordinary and achieve success beyond boundaries. At, ES / EAIS unit, your efforts towards creating an environment of trust, driving a lot of self-respect and independence, meant so much to the team and you could effortlessly rally your troops to create outstanding performance for the unit. Your passionate leadership had set the tone for a fantastic journey ahead! A people’s leader par excellence, you have always been a great friend, passionate for people and a source of positive influence. Wish you the very best for a fantastic time ahead – Gopinator & the Navigator! Thanks, Srini",
                "EmpName": "Srini",
                "Unit": "FRIEND",
                "Pic Details": "FRIEND2",
                "Order": "101",
                "image": "35pic.png"
            },
            {
                "Comments": "Dear Gopi, Congratulations on your 25th year of association with Infosys. You have set an incredible example of leadership with passion to do better every day and inspire people. Knowing you for the last 2 decades I have always admired the way you have made a difference in the life of so many people by mentoring on their professional growth, counselling, empathizing and going out of the way to help people in very tough situations – personal or professional. It takes tremendous amount of energy, intensity and passion which someone like me can only imagine. I probably have not come across anyone very close to me who has been through ups and downs both personally and professionally but so courageous to face it, manage and set an example… a maybe I would think some day you will write a book ! Lastly, while I have worked with many bosses I have never come across anyone who I can share anything professionally without any fear or perceptions or consequences. I wish you and your family the very best, full of happiness and even greater success in life. I consider 25th anniversary as Gopi 1.0 and I look forward Gopi 2.0. Congratulations!!!",
                "EmpName": "Vinayak Hegde",
                "Unit": "FRIEND",
                "Pic Details": "FRIEND6",
                "Order": "102",
                "image": "36pic.PNG"
            },
            {
                "Comments": "\"Gopi , hearty Congratulations on entering the special 25 years club in Infy. For the kind of time, superhuman effort and commitment you have given to Infosys, it wouldn’t be an exaggeration to equate this to 100 years of any normal person would have given. You have been a rare breed of those leaders who can inspire and ignite passion to achieve higher goals. For me personally you have been a mentor whom I have looked up to and an inspiration to draw the mental strength needed for facing the toughest challenges that life offers. Thanks for everything and here’s to next 25. - Manju \"",
                "EmpName": "Manju",
                "Unit": "FRIEND",
                "Pic Details": "DC",
                "Order": "103",
                "image": "37pic.png"
            },
            {
                "Comments": "Dear Gopi, Congratulations on this amazing feat! 25 years and still young and going very strong! Really something for everyone to emulate! Wishing you many more successful years at Infosys. Regards, Anil Kumar P",
                "EmpName": "Anil Kumar P",
                "Unit": "FRIEND",
                "Pic Details": "FRIEND5",
                "Order": "104",
            },
            {
                "Comments": "\"Dear Gopi, Congratulations on your anniversary! Thank you so much for your empathetic leadership and always keeping the best interest of the team at heart ! Working with you, I learnt the pursuit of excellence and differentiation. We were so comfortable with you even while we would make mistakes. Your compassionate leadership left a deep mark on me. You always left us feeling special, inspired and charged up to do bigger and better things in every interaction. I feel fortunate for getting the opportunity to work with you and benefit under your leadership. Thank you for being with us. Wish you immense success and happiness for the years to come ! \"",
                "EmpName": "Kapil Goel",
                "Unit": "FRIEND",
                "Pic Details": "FRIEND2",
                "Order": "105",
                "image": "39pic.png"
            },
            {
                "Comments": "Hi Gopi, Many Congratulations on this stupendous milestone in your career! Wish you many more successful years at Infy! And you are rocking this green kurta :) - Prabhat",
                "EmpName": "Prabhat Kaul",
                "Unit": "FRIEND",
                "Pic Details": "FRIEND1 AND SOLO13",
                "Order": "106",
            },
            {
                "Comments": "Dear Gopi, You have been a role model for most of us as a leader - someone who is very approachable and always ready to guide and help. Glad to have your acquaintance !! Saket",
                "EmpName": "Saket Singh",
                "Unit": "FRIEND",
                "Order": "107",
            },
            {
                "Comments": "What a ride it’s been Gopi! Wishing you the very best! Regards, sampath",
                "EmpName": "Sampath",
                "Unit": "FRIEND",
                "Pic Details": "DC7",
                "Order": "108",
                "image": "42pic.png"
            },
            {
                "Comments": "Gopi Saar I have known you for only 5 years out of the 25 and how I wish I'd met you earlier! Congratulations on this momentous occassion and wishing you the very best for the future! Cheers Sanchit",
                "EmpName": "Sanchit Mullick",
                "Unit": "FRIEND",
                "Pic Details": "LEADERSHIP4",
                "Order": "109",
            },
            {
                "Comments": "Hearty Congratulations, Gopi. It has been a privilege knowing you for the last 21 of these 25 years. Wish You all the very best as you complete this extraordinary milestone. Keep continuing to be the great person that you are. -Sheethal",
                "EmpName": "Sheethal Kurien",
                "Unit": "FRIEND",
                "Pic Details": "DC1",
                "Order": "110",
                "image": "44pic.jpg"
            },
            {
                "Comments": "It is not the time you spend with a company, but the way you spend this time with people surrounding you that brings meaning, hope, inspiration and motivation to the teams. You are a very special leader, one that I have only read in books - An embodiment of integrity, leadership by example, unwavering focus on business all while being compassionate and considerate. Happy 25th and I wish you many more successful years with Infosys! Sri Challa",
                "EmpName": "Srikanth Challa",
                "Unit": "FRIEND",
                "Order": "111",
            },
            {
                "Comments": "Congratulations Gopi on completing 25 years at Infosys. Infosys is very fortunate to have you as one of their Leader, where you have got immense value into the organization in all spheres of your professional/personal engagements. You have always been an inspiration for me throughout my professional journey at Infosys. You put in selfless effort to bring value to whatever you lead at Infosys. I keenly listen to your point of views that you share in any of the addresses that you do at Infosys. Here is wishing you many more successful years at Infosys and wish all your aspirations be fulfilled.",
                "EmpName": "Randhir Manjeshwar",
                "Unit": "EAIS",
                "Order": "201",
                "image": "46pic.PNG"
            },
            {
                "Comments": "Dear Gopi, Congratulations for your 25th Work Anniversary at Infosys. Appreciate your contagious enthusiasm, empathetic leadership and never give up attitude! Hope your future years are as great as what you have had before! Best Wishes! Regards, Smriti",
                "EmpName": "Smriti G",
                "Unit": "EAIS",
                "Order": "202",
                "image": "47pic.png"
            },
            {
                "Comments": "Hi Gopi, Congratulations!!! you are an embodiment of Infosys Values and leadership!!! i hope we can all learn from you. - Susanto",
                "EmpName": "Susanto Kumar De",
                "Unit": "EAIS",
                "Order": "203",
                "image": "48pic.png"
            },
            {
                "Comments": "Dear Gopi , Congratulations on this Fantastic Milestone .You have been truly Inspirational Leader .Thanks for your support and guidance . Best Regards, Thangappan",
                "EmpName": "Thangappan",
                "Unit": "EAIS",
                "Order": "204",
                "image": "49pic.png"
            },
            {
                "Comments": "Dear Gopi, Many Congratulations! That you have been here for 25 years says it all. You are truly inspiring to all of us. Wishing you a happy work anniversary! Regards, Alok Ghosh",
                "EmpName": "Alok Kumar Ghosh",
                "Unit": "ORC",
                "Order": "301",
                "image": "50pic.png"
            },
            {
                "Comments": "Congratulations Gopi on a stupendous innings at Infosys thus far!!! Wishing you the very best for many more fabulous years ahead at Infy !!! Cheers, Alok Bose",
                "EmpName": "Alokmoy Bose",
                "Unit": "ORC",
                "Pic Details": "ORC 1 TO 6",
                "Order": "302",
                "image": "51pic.PNG"
            },
            {
                "Comments": "Dear Gopi, Hearty Congratulations on your Silver Anniversary with Infosys! Thank you for being an inspirational leader and mentor! Regards, Arun Thantry",
                "EmpName": "Arun Kumar Thantry",
                "Unit": "ORC",
                "Order": "303",
            },
            {
                "Comments": "Congratulations Gopi for a stellar tenure of 25 years with Infosys. Looking forward to your continued success in the organization, reaching newer heights in coming years. - Avrajyothi",
                "EmpName": "Avrajyoti Chakraborty",
                "Unit": "ORC",
                "Order": "304",
            },
            {
                "Comments": "Hi Gopi, Congratulations! Your leadership is exemplary and you're a role model for many of us. Your energy, your positivity, people orientation is inspirational. Wishing you many more years at Infosys. - Deepak Mandot",
                "EmpName": "Deepak Mandot",
                "Unit": "ORC",
                "Order": "305",
            },
            {
                "Comments": "Many Congratulations Gopi on your 25th Service Anniversary. Feel Great to be part of your team. Thanks for all your support. Wishing you all the very best. Regards Hari",
                "EmpName": "Hari Subramanian P. K.",
                "Unit": "ORC",
                "Order": "306",
            },
            {
                "Comments": "Dear Gopi - Hearty Congratulations for this wonderful milestone GOD bless you! Thank you Jacob Mathew",
                "EmpName": "Jacob Mathew",
                "Unit": "ORC",
                "Order": "307",
            },
            {
                "Comments": "Gopi, congratulations for your 25th anniversary in the company. You are an example to follow in every way and I am very happy to work for a great professional and a person like you. I am proud to be Infosys for the great company and for people like you who have the highest level worldwide. All the best. Marlon Thompson.",
                "EmpName": "Marlon Thompson",
                "Unit": "ORC",
                "Order": "308",
            },
            {
                "Comments": "Congrats Gopi, wishing you many more success in the years ahead. - Mohan",
                "EmpName": "Mohan Subramanian",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Hi Gopi, Congratulations for completing a fantastic and successful quarter century!! Wishing the organization enjoys you inspirational leadership for several more years to come. Thanks & Regards, Mukesh Vadhyar",
                "EmpName": "Mukesh Vadhyar",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Congratulations Gopi on serving Infosys for over a quarter of a century. Your vivacious and gregarious nature attracts the people around you. Wishing you success and godspeed in all the endeavors that you undertake. - Murali",
                "EmpName": "Muralidharan Srinivasan",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Dear Gopi It has been an absolute pleasure working with you. Thank you for being there when required and providing all the support needed, above all being a good friend and mentor Regards Newin",
                "EmpName": "Newin Durai",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Dear Gopi, It's been privilege to work under your exemplary leadership. You are great inspirational leader and above all great human being. Wishing you many more successful years at Infosys. Best Regards, Nukul",
                "EmpName": "Nukul Dinkar",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Hi Gopi, Congratulations on achieving this big milestone. All the very best for the coming years aheead. - Pankaj",
                "EmpName": "Pankaj Kalia",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "From the solemn nods to empathic discussions, From carefree laughter to serious deliberations; From strong convictions to gentle submissions, From an invictus warrior to a humble human. That’s Gopi for you, Innovator – pushing his people beyond the boundaries of their own imagination Motivator – challenging each self-imposed constraint and asking men to brave the tide Gopinator – making us believe, through your virtuous leadership, that a someone can be as humane, caring and compassionate Extremely proud to be your team!!! Congratulations and may we continue to have many more years of your inspiring leadership!!! - Prerna",
                "EmpName": "Prerna Sharma",
                "Unit": "ORC",
                "Order": "309",
                "image": "64pic.jpg"
            },
            {
                "Comments": "\"Dear Gopi, Hearty congratulations on the silver jubilee. You have been an inspiration in various ways. I still remember how you magically created a lively environment in Kankanady office! Wish you the very best!...Thanks Radhakrishnan. \"",
                "EmpName": "Radhakrishnan P K",
                "Unit": "ORC",
                "Order": "309",
                "image": "65pic.jpg"
            },
            {
                "Comments": "Congratulations Gopi on this amazing milestone. With the passion you bring every day, its very inspiring to work with you as we scale up Oracle practice to greater heights. Thank you for your unstinted support and continuous guidance - Raghu",
                "EmpName": "Raghu Boddupally",
                "Unit": "ORC",
                "Order": "309",
                "image": "66pic.jpg"
            },
            {
                "Comments": "Congratulations Gopi on completing this milestone. Thank you for your support and mentorship for last 10+ years of association and the opportunity that I got to work with you in this journey. All the Best for another 10 and more.....Rajesh Dubey",
                "EmpName": "Rajesh K. Dubey",
                "Unit": "ORC",
                "Order": "309",
                "image": "67pic.jpg"
            },
            {
                "Comments": "Congratulations Gopi on turning 25. Many more to come with loads of Success - Ramakrishnan",
                "EmpName": "Ramakrishnan Subramanian",
                "Unit": "ORC",
                "Order": "309",
                "image": "68pic.jpg"
            },
            {
                "Comments": "Hearty congratulations on your 25th Anniversary, Gopi! - Ravi Appayya",
                "EmpName": "Ravi Appayya",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Congratulations on your Silver Jubilee celebrations at Infosys. You are the cornerstone of Oracle practice and I wish you many more years at Infosys!! - Rohit Kumar",
                "EmpName": "Rohit Kumar",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Congratulations Gopi on this great milestone. Wish you many more years of success. Regards Rohit Rastogi",
                "EmpName": "Rohit Rastogi",
                "Unit": "ORC",
                "Order": "309",
                "image": "71pic.jpg"
            },
            {
                "Comments": "Hi Gopi Congratulations for the milestone . Thank you for your leadership and guidance. Wish you many more successful years ahead Regards Sameer Munje",
                "EmpName": "Sameer Arun Munje",
                "Unit": "ORC",
                "Order": "309",
                "image": "72pic.jpg"
            },
            {
                "Comments": "Hi Gopi, What a stellar innings in Infosys you have had. Congrats on this successful milestone and wishing you many more years at Infosys and in steering the Oracle ship onward and forward. - Sanjay",
                "EmpName": "Sanjay Pinto",
                "Unit": "ORC",
                "Order": "309",
                "image": "73pic.jpg"
            },
            {
                "Comments": "Dear Gopi, 25 Years is indeed a long time and I am sure there would be many reasons to cherish this journey. We got exposed to your energetic personality during Vishl's ZD Sessions, and not got opportunity to work closer as our leader for Oracle Practice. Thank you for your support and guidance and 'protecting' us from the broader heat on various matters we don't do well. Best Wishes, Sanjeev Pendse.",
                "EmpName": "Sanjeev Sudhakar Pendse",
                "Unit": "ORC",
                "Order": "309",
                "image": "74pic.jpg"
            },
            {
                "Comments": "Gopi Sir, You are inspirational! Congratulations for 25 successfully years in Infosys. We are lucky to have you here. Regards, Shrawan",
                "EmpName": "Shrawan Mangalam",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Many Congratulations on your Silver Jubilee with Infosys! You have been a role model for a lot of us @ Infosys. Wishing you all the best for your next 25 @ Infosys. Regards, Somnath",
                "EmpName": "Somnath Majumdar",
                "Unit": "ORC",
                "Order": "309",
                "image": "76pic.png"
            },
            {
                "Comments": "Dear Gopi Congratulations on this remarkable career milestone. You were my first manager when I joined Infy 17 years ago and now life has come full circle when you again took over as Oracle unit head last year. You are one of the most enthusiastic and passionate individual who always put the organization above self. It has been an absolute pleasure interacting with you both on a professional and personal mode. Wish you a very successful career and a happy and contended life. - Sreekumar",
                "EmpName": "Sreekumar Sreedharan",
                "Unit": "ORC",
                "Pic Details": "ORC6",
                "Order": "309",
                "image": "77pic.jpg"
            },
            {
                "Comments": "Gopi, congratulations on your Silver Jubilee at Infosys. Wish you many more successful years in Infosys. - Srikanth Sripathi",
                "EmpName": "Srikanth Sripathi",
                "Unit": "ORC",
                "Order": "309",
                "image": "78pic.jpg"
            },
            {
                "Comments": "Congratulations Gopi! All the best for 25 more :-)! - Srinivas Kolipakkam",
                "EmpName": "Srinivas Kolipakkam",
                "Unit": "ORC",
                "Order": "309",
                "image": "79pic.jpg"
            },
            {
                "Comments": "Dear Gopi, Hearty congratulations on completing 25 years at Infosys. We met when I was at WWT. Hope you remember me :-) Best Wishes Srini Mantripragada SME at Harmonic.",
                "EmpName": "Srinivas Mantripragada",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Dear Gopi, Thanks for guiding and steering Oracle practice. Wish you many such milestones in Infosys. Regards, Sriram Devanathan",
                "EmpName": "Sriram Devanathan",
                "Unit": "ORC",
                "Order": "309",
            },
            {
                "Comments": "Congratulations Gopi for this wonderful milestone … - Tito Jose",
                "EmpName": "Tito Jose Moothedan",
                "Unit": "ORC",
                "Order": "309",
                "image": "82pic.jpg"
            },
            {
                "Comments": "Hi Gopi, Congratulations for being a special leader for so many years. Oh my God! 25 years in Infosys..!! Your dedication and loyalty will be forever cherished. Even though it’s not been so long time for you heading our Oracle practice, we are so proud to have you here in as good role model to all. More fruitful years to come in your career & May you continue to be a remarkable leader in Infosys. I wish you good luck! - VInay Jain",
                "EmpName": "Vinay Kumar Jain",
                "Unit": "ORC",
                "Order": "309",
                "image": "83pic.jpg"
            },
            {
                "Comments": "I just attended one session of yours in the HRD EC offsite in Mangalore and could get the nerve of your leadership. You are an inspiration Gopi, keep rocking and looking forward to your leadership in taking Infosys to greater heights. Thanks!! Regards, Anirudh",
                "EmpName": "Anirudhan Vasudevan",
                "Unit": "HR",
                "Order": "400",
                "image": "84pic.png"
            },
            {
                "Comments": "Dear Gopi, You have been inspirational as a leader and as person. Being around you, I have learnt that one can achieve what they wish if they have the passion and the drive. Your energy and positivity in our interactions has always encouraged, motivated and made me more confident . I look forward to the next inspirational and radiant 25 years . Happy Anniversary Gopi.",
                "EmpName": "Anthia",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "85pic.png"
            },
            {
                "Comments": "Hearty congratulations on achievement of this splendid milestone Gopi. wishing you the best for many more to come! Regards, Deepa Badi",
                "EmpName": "Deepa Badi",
                "Unit": "HR",
                "Order": "400",
                "image": "86pic.png"
            },
            {
                "Comments": "Your ability to evolve as a person and leader is the most inspiring aspect of you. The ability to show vulnerability, be humane and task oriented in your interactions make the relationships authentic. In a race where the rats run its a delight to find a human. A leader on top of that is pure delight. Thank you for being you. - Deepa Premkumar",
                "EmpName": "Deepa Premkumar",
                "Unit": "HR",
                "Order": "400",
                "image": "87pic.png"
            },
            {
                "Comments": "\"Dear Gopi, I recall you singing ‘Eena, Meena, Dika’ at my cubie a long time ago. Since then, it has been a wonderful association with you. More specially, my first role as a HR Manager was for ADT where you gave me a lot of freedom to experiment things. Having you as the Unit Head for the Oracle practice has been a blessing cause closures, decisions are so swift and fast and you have infused the Unit with new energy and enthusiasm. The best part of my association with you is knowing you as a person – someone who is a meticulous planner whether it’s a fun event or the most serious of business meetings, a Leader who loves to connect with people and one who genuinely cares, and above all, a person always rooted to the ground. On this momentous occasion, wishing you good health, joy and happiness on your 25th milestone in Infosys and hoping more success comes your way in the years to come. Stay Blessed always Gopi Best wishes, Dinha \"",
                "EmpName": "Dinha",
                "Unit": "HR",
                "Pic Details": "HR2",
                "Order": "400",
                "image": "88pic.png"
            },
            {
                "Comments": "Congratulations on achieving the 25 year milestone!! Stay awesome :) - Hima",
                "EmpName": "Hima Madan",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "89pic.png"
            },
            {
                "Comments": "Dear Gopi, it has been a pleasure to not just work with you but to also have you as a leader who would be your good friend. Thank you for always being generous in all your dealings and interactions. I wish you the best in everything you do.",
                "EmpName": "Jacinta",
                "Unit": "HR",
                "Order": "400",
                "image": "90pic.png"
            },
            {
                "Comments": "Its been 1.5 years since i am working with you, Your professionalism and down to earth attitude has made work seem good around here:-)We will always pray for your happiness, good health and success. - Marlin",
                "EmpName": "Marlin Pinto",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "91pic.png"
            },
            {
                "Comments": "Congratulations... a long innings but an eventful one... wishing you the very best to scale even greater heights. Thank you for everything. - Rags",
                "EmpName": "Raghavendra K",
                "Unit": "HR",
                "Order": "400",
            },
            {
                "Comments": "Congratulations on this big milestone Gopi! May you continue to shine and be an inspiration all of us at Mangalore DC! :) Regards Ragini",
                "EmpName": "Ragini Rabindra",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "93pic.png"
            },
            {
                "Comments": "\"Hearty congratulations Gopi on your silver jubilee milestone! You have made Infosys a better place to work for many infoscions over these years. Thank you for your leadership. Best wishes for many more successful and fulfilling years ahead. Best Wishes, Rajesh \"",
                "EmpName": "Rajesh Kanan",
                "Unit": "HR",
                "Order": "400",
                "image": "94pic.png"
            },
            {
                "Comments": "Dear Gopi, Congratulations as you complete 25 glorious years at Infosys. It's been insightful working with you always.Thanks for your guidance, mentoring and leadership one looks up to. Best Regards, Ram",
                "EmpName": "RamaPrasad",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "95pic.png"
            },
            {
                "Comments": "Thank you Gopi for being the Mentor, Confidant, Friend and Teacher all in one. Every conversation has added immense value in my personal and professional growth and given me strength when I needed it the most. Take time and feel all the pride and happiness surrounding you. I’m sure today is one of the many proud moments. Cheers to a Remarkable and Inspirational journey. - Reshma Monteiro",
                "EmpName": "Reshma Monteiro",
                "Unit": "HR",
                "Pic Details": "HR3",
                "Order": "400",
                "image": "96pic.png"
            },
            {
                "Comments": "\"Dear Gopi, Congratulations on this Milestone! It has been an absolute honor working with you. Your ability to evolve as a wonderful person and a great leader is an inspiring aspect of you. Wishing you the very best for the years ahead! – Sandeep Dsilva \"",
                "EmpName": "Sandeep DSilva",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "97pic.png"
            },
            {
                "Comments": "Congratulations Gopi on completion of 25 fabulous years with Infy. What an awesome journey! Thank you for your leadership and support. Wishing you the very best and all the success ahead! Regards Santosh Panpaliya",
                "EmpName": "Santosh Panpaliya",
                "Unit": "HR",
                "Pic Details": "LEADERSHIP9",
                "Order": "400",
                "image": "98pic.png"
            },
            {
                "Comments": "Many Congratulations Gopi on accomplishing this wonderful milestone. You have been a true blue Infoscion and a role model for many in the Organization. Amazing career journey, spectacular success but yet very grounded and humble. Above all a genuine person to work with and great to know you as a person. Wishing you many such successful years ahead. Satish Kannan",
                "EmpName": "Satish Kannan",
                "Unit": "HR",
                "Order": "400",
            },
            {
                "Comments": "An Awesome Human Being, filled with abundance of positive vibe and energy. Kudos to your 25 amazing years of work Gopi., & Thank you for being the true Blue “YOU”. Cheers to many many more. SHEEMA",
                "EmpName": "Sheema Tasneem",
                "Unit": "HR",
                "Pic Details": "LEADERSHIP10",
                "Order": "400",
                "image": "100pic.png"
            },
            {
                "Comments": "A very hearty Congratulations Gopi on achieving this wonderful milestone. Wishing you many more years of success. - Suchetha",
                "EmpName": "Suchetha Bhaktha",
                "Unit": "HR",
                "Pic Details": "HR",
                "Order": "400",
                "image": "101pic.png"
            },
            {
                "Comments": "Many Congratulations Gopi Bhai on completing 25 glorious years in Infosys. Its always a pleasure to work with you and also listen to you. Your passion, attitude and approach to work is what makes you the best. You have been a huge influence on me and multiple time you have given the best professional advice to me. Fortunate to have you as a friend, guide and mentor. Looking forward to work with you continuously. Wish you all the best for many more years of success. Regards Sudheer",
                "EmpName": "Sudheer Pai C",
                "Unit": "HR",
                "Pic Details": "DC12",
                "Order": "400",
                "image": "102pic.png"
            },
            {
                "Comments": "Hi Gopi, It has been very good working with you, your support to HR team at location is awesome and the inspiration you get to folks in the unit/DC is commendable, Congratulations on silver jubilee and wish you all the success in the years to come. - Vijaya Praveena",
                "EmpName": "Vijaya Praveena",
                "Unit": "HR",
                "Pic Details": "HR2",
                "Order": "400",
                "image": "103pic.png"
            },
            {
                "Comments": "Dear Gopi - Thanks for your wonderful contributions to Infosys. Pleasure working with you. Wishing you the very best for the years ahead! - Balaji S",
                "EmpName": "Balaji S",
                "Unit": "MKTG",
                "Order": "500",
            },
            {
                "Comments": "Mr GopiKK, Gopikrishnan Konnanath, friend, colleague, mentor, father of Pachu and Hari, uncle to king Ramu. A leader, a reader, a great story teller, an empath, a great man. 25 years you have devoted to Infosys. In return you have inherited thousands of people that think of you as highly as I do. What inspires me the most is how you bring out the very best in everyone you work with. You have a strong mental map of how things should be done and it always involves integrity, hard work and collaboration. You are systematic and practical while at the same time creative. Assertive yet open-minded. Today we are not only celebrating the 25 years you have spent working at Infosys, but the 25 years you have spent building long lasting relationship with people who value and adore you. sincerly, Dena",
                "EmpName": "Dena Tahmasebi",
                "Unit": "MKTG",
                "Order": "500",
            },
            {
                "Comments": "Dear Gopi, Congratulations on this milestone! You are an inspiration! Thank you so much for being a fantastic leader! All the best for many more rocking years ahead. Best wishes, Jennifer",
                "EmpName": "Jennifer Roshni Rego",
                "Unit": "MKTG",
                "Order": "500",
            },
            {
                "Comments": "Hi Gopi, It has been a pleasure so far working under your leadership. I especially like the non-interfering nature of working and letting people be. Please keep that josh and that smile on...many congratulations for 25 years. That's almost a lifetime! best regards amit vats",
                "EmpName": "Amit Vats",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Gopi Congratulations. All the very best for decade and more. You all have built an organisation of repute. Your diligence, hardwork and commitment have touched many. We can just say, thank you! Warm Regards Ashok Hegde FS DCG",
                "EmpName": "Ashok Hegde",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Hey Gopi, congratulations on this fantastic journey at Infosys. Stay the way you are. God bless and Here's to many more !!! - Atul Sahgal",
                "EmpName": "Atul Sahgal",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi, Congratulations on an incredible achievement! Wishing you many more years of success with Infosys ahead. Warm regards, Avery Klurfield",
                "EmpName": "Avery Klurfield",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi Its been absolutely wonderful being with you. You've been a perfect role model and an inspiration to everyone who has had the privilege of getting to know you. Wishing you many many more glittering milestones ahead Regards Debashish",
                "EmpName": "Debashish Kumar Ganguly",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Many congratulations on this momentous occasion. An illustrious career and a glorious milestone. Your dedication, enthusiasm and creative vision is always inspiring. Wishing you the very best, Gopi!! - GLNRAO",
                "EmpName": "GLN Rao",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Congratulations, Gopi!!! Wish you all the best! Regards Jay",
                "EmpName": "Jayakumar PK",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Gopi, you are always amazingly cheerful and professional. Great knowing you and enjoy your wonderful milestone!! Cheers, Manohar",
                "EmpName": "Manohar M. Atreya",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Hi Gopi, Congratulations on your anniversary. It is a tremendous pleasure to work with you. All the best and warmest wishes as well as continuous success. Regards, Maria Didorenko",
                "EmpName": "Maria Didorenko",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Hi Gopi, Congratulations for completing 25 yrs at Infosys. It has been a pleasure working with you in Mangalore. Thank You for all your guidance and mentoring. RRR",
                "EmpName": "Rajiv Ranjan Rai",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Gopi - You are a wonderful friend, great colleague and overall a genuine person, loved equally by your teams, peers and bosses for what you are. Congratulations on this wonderful occasion. Wish you many more successful years with Infosys. Keep smiling and stay happy !! Ramesh Amancharla",
                "EmpName": "Ramesh Amancharla",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi Hearty Congratulations on this amazing milestone! It has been a great experience working with you and learning from you. You are an inspiring leader and I wish you all the very best as you continue your leadership journey at Infosys! Regards, Ruchika",
                "EmpName": "Ruchika Jain",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "You are synonymous with Mangalore and the fantastic experience every employee gets there. Wish you all good things in life at Infosys and outside. - Sri",
                "EmpName": "Srividhya V S",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi, Across the continents wherever we are in Infosys, your leadership always inspires. --Suman Kanth",
                "EmpName": "Suman Kumar Kanth",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Congratulations and welcome to the club, my friend. I am always amazed at your energy and leadership. All the best!. Regards, Sunil Jose",
                "EmpName": "Sunil Jose",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "hi Gopi, We came to know each other only a few years back, but we immediately connected it became a good friendship. Keep being the positive, committed, resilient, open and friendly person that you are. Wish you the very best! regards Sushil K",
                "EmpName": "Sushil K",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi, Heartiest Congratulations on this amazing milestone! 25 years! Great commitment and track record of valuable contribution to the company! It has been an absolute pleasure working with you on various European initiatives and also on BTN! Looking forward to celebrating many more such milestones with you! Godspeed! With Best Regards, Sushrut",
                "EmpName": "Sushrut Vaidya",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Gopi has been a pillar of Infosys and has donned a diversity of roles here. Hes become so synonymous with Mangalore DC that it seems difficult to visualize the vibrant and beautiful DC without him in it. On a personal note Gopi you are a fantastic guy, young at heart and thought, rooted in Infy values, a never-say-die-attitude, and the eagerness to experiment. Above all, a fun guy. Wish you the best for 30 and beyond. Cheers! - Vardha",
                "EmpName": "Vardha",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Dear Gopi Congratulations on this milestone. Time flies I remember we celebrated your 20th anniversary in Mnglr together. All the best for many more to come. Been a pleasure working with you. - VJ",
                "EmpName": "Vijayanush Narasimhan",
                "Unit": "last",
                "Order": "600",
            },
            {
                "Comments": "Hi Gopi, Congrats on achieving this milestone! It has been a cherished and memorable experience to be led by an understanding, connected and empathetic Leader like you. I look forward to your inputs and guidance in years to come. Thanks and Regards- Vivek",
                "EmpName": "Vivek Goswami",
                "Unit": "last",
                "Order": "600",
            }
        ]
    }

export default wishes;